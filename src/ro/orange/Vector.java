package ro.orange;

import java.util.Scanner;

public class Vector {

    public static void main(String[] args) {
        int n;
        Scanner sc = new Scanner(System.in);    // scanner used for parsing

        System.out.print("Enter the number of elements n= ");
        n = sc.nextInt();

        int[] V = new int[n];

        int i = 0;
        while (i < n) {
            System.out.print("V[" + i + "]=");
            V[i] = sc.nextInt();
            i++;
        }
        for (int elem : V) {                    // foreach
            System.out.println(elem);
        }
    }
}
